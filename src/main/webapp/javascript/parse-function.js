Parse.initialize("DIW1pOcIJIXQ8ZWoYlU6HRuJMiXfT3i8yCIJhswJ", "3HzERtKh6ulsedfAWEk2IPkX62wIWpgArVF9FJ0c");

if (!navigator.userAgent.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry)/)) {
	addData();
	addRest();
} else {
	document.addEventListener("deviceready", addData, false);
	document.addEventListener("deviceready", addRest, false);
}

function addRest() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "https://api.parse.com/1/classes/GameScore",
        dataType: "json",
		data: formToJSON(),
		beforeSend: setHeader,
        success: function(data, textStatus, jqXHR){
            $("#store_success_rest").show();
			$("#store_success_rest").append(data.createdAt + " " + data.objectId + " " + textStatus);
        },
        error: function(jqXHR, textStatus, errorThrown){
            $("#store_error_rest").show();
			$("#store_success_rest").append(textStatus + " " + errorThrown);
        }
    });
	
	$.ajax({
		url: "https://api.parse.com/1/classes/GameScore/Ngxkw1pXaZ",
		beforeSend : setHeader,
		success: function(gameScore){
			$("#retreive_success_rest").show();
			var score = gameScore.score;
			var playerName = gameScore.playerName;
			var cheatMode = gameScore.cheatMode;
			var deviceName = gameScore.deviceName;
			var deviceUUID = gameScore.deviceUUID;
			var objectId = gameScore.objectId;
			var updatedAt = gameScore.updatedAt;
			var createdAt = gameScore.createdAt
			$("#retreive_success_rest").html("<p>We've also just retreived the object:<br />Score:" + score + " Player Name:" + playerName + "cheatMode:" + cheatMode + "device name:" + deviceName + "deviceUUID:" + deviceUUID + "objectId:" + objectId + "updatedAt:" + updatedAt + "createdAt:" + createdAt + "</p>")
			}
		});	
}

function setHeader(xhr) {
	xhr.setRequestHeader("X-Parse-Application-Id", "DIW1pOcIJIXQ8ZWoYlU6HRuJMiXfT3i8yCIJhswJ");
	xhr.setRequestHeader("X-Parse-REST-API-Key", "TBwH4l3TrNB1AKT9nhRfR2kLmOAMEtpOujo1vu5O");
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	var deviceName = "";
	var deviceUUID = "";
	var deviceVersion = "";
	
	if (!navigator.userAgent.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry)/)) {
		deviceName = "desktop";
		deviceUUID = "0";
		deviceVersion = jQuery.browser.version;
	} else {
		deviceName = device.name;
		deviceUUID = device.uuid;
		deviceVersion = device.version;
	}
	
    return JSON.stringify({
		"score": 1337,
		"playerName": "Sean Plott",
		"cheatMode": false,
		"deviceName" : deviceName,
		"deviceUUID" : deviceUUID,
		"deviceVersion" : deviceVersion,
        });
}

function addData() {
	// Save Data
	var GameScore = Parse.Object.extend("GameScore");
	var gameScore = new GameScore();
	var deviceName = "";
	var deviceUUID = "";
	var deviceVersion = "";

	if (!navigator.userAgent.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry)/)) {
		deviceName = "desktop";
		deviceUUID = "0";
		deviceVersion = jQuery.browser.version;
	} else {
		deviceName = device.name;
		deviceUUID = device.uuid;
		deviceVersion = device.version;
	}

	gameScore.save({
		score: 1337,
		playerName: "Sean Plott",
		cheatMode: false,
		deviceName : deviceName,
		deviceUUID : deviceUUID,
		deviceVersion : deviceVersion,
	}, {
		success: function(gameScore) {
			$("#store_success").show();
			$("#store_success").append(gameScore.createdAt + " " + gameScore.id);
		},
		error: function(gameScore, error) {
			// The save failed.
			// error is a Parse.Error with an error code and description.
			$("#store_error").show();
		}
	});

	// Retreive Data
	var GameScore = Parse.Object.extend("GameScore");
	var query = new Parse.Query(GameScore);
	query.get("Ngxkw1pXaZ", {
		success: function(gameScore) {
			$("#retreive_success").show();
			var score = gameScore.get("score");
			var playerName = gameScore.get("playerName");
			var cheatMode = gameScore.get("cheatMode");
			var deviceName = gameScore.get("deviceName");
			var deviceUUID = gameScore.get("deviceUUID");
			var objectId = gameScore.id;
			var updatedAt = gameScore.updatedAt;
			var createdAt = gameScore.createdAt
			$("#retreive_success").html("<p>We've also just retreived the object:<br />Score:" + score + " Player Name:" + playerName + "cheatMode:" + cheatMode + "device name:" + deviceName + "deviceUUID:" + deviceUUID + "objectId:" + objectId + "updatedAt:" + updatedAt + "createdAt:" + createdAt + "</p>")
		},
		error: function(object, error) {
			$("#retreive_error").show();
			if (error.code === Parse.Error.OBJECT_NOT_FOUND) {
				$("#retreive_error").html("Uh oh, we couldn't find the object!");
			} else if (error.code === Parse.Error.CONNECTION_FAILED) {
				$("#retreive_error").html("Uh oh, we couldn't even connect to the Parse Cloud!");
			} else {
				$("#retreive_error").html("An unknown error occured!");
			}
		}
	});
}